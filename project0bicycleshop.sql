create table person (
	id serial primary key,
	username varchar(30) unique not null,
	passwd varchar(30) not null,
	user_role_id integer references person_role
);

create table person_role (
	id serial primary key,
	name varchar(10) unique not null
);


insert into person_role values
	(1, 'Employee'),
	(2, 'Customer'),
    (2, 'Manager');



create table status (
         id serial primary key,
         name varchar(10) unique not null
         );
        
     
        

insert into status values
	(1, 'Available'),
	(2, 'Sold');

create table bicycle (
         id serial primary key,
         reference varchar(50) unique not null,
         color varchar(50) not null,
         size_b float not null,
         price float not null,
         brand varchar(50) not null,
         status_id integer references status,
         );
        
    


create table offer_status (
         id serial primary key,
         name varchar(10) unique not null
         );
         
insert into offer_status values
	(1, 'Pending'),
	(2, 'Accepted'),
	(3, 'Rejected');
	

create table offer (
         id serial primary key,
         amount varchar(10) unique not null,
         person_id integer references person ON DELETE CASCADE ON UPDATE CASCADE,
         bicycle_id integer references bicycle ON DELETE CASCADE ON UPDATE CASCADE,
         offer_status_id integer references offer_status 
        
         );
         



alter table offer drop bicycle_id;
alter table offer add constraint bicycle_id_fkey foreign key (bicycle_id) references bicycle (id) on update no action on delete cascade;



alter table offer add bicycle_id integer references bicycle;


alter table offer
DROP CONSTRAINT offer_offer_status_id_fkey;

alter table payment
add week_remaining integer;
  


create table payment (
         id serial primary key,
         amount_to_pay Float,
         remaining_amount Float,
         offer_id integer references offer ON DELETE CASCADE ON UPDATE CASCADE
         );
        
